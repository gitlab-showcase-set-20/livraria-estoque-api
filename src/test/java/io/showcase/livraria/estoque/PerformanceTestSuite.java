package io.showcase.livraria.estoque;

import io.showcase.livraria.estoque.performance.PerformanceTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({PerformanceTests.class})
public class PerformanceTestSuite {
}
