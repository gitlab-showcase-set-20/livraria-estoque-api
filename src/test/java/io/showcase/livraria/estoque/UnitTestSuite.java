package io.showcase.livraria.estoque;

import io.showcase.livraria.estoque.service.EstoqueServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({EstoqueServiceTest.class})
public class UnitTestSuite {
}
