package io.showcase.livraria.estoque.service;

import io.showcase.livraria.estoque.dto.EstoqueInfo;
import org.springframework.stereotype.Service;

@Service
public class EstoqueService {

    public EstoqueInfo getEstoqueByProduto(Long productId) {
        if (productId == 99L) {
            return new EstoqueInfo(productId, 99L);
        }
        return new EstoqueInfo(productId, 0L);
    }

    public int calculate(int a) {
        return a + 1;
    }

}
