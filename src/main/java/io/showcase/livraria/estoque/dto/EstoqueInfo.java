package io.showcase.livraria.estoque.dto;

public class EstoqueInfo {

    private Long productId;
    private Long quantity;


    public EstoqueInfo() {
    }

    public EstoqueInfo(Long productId, Long quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
