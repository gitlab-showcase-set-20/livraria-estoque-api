package io.showcase.livraria.estoque.controller;

import io.showcase.livraria.estoque.dto.EstoqueInfo;
import io.showcase.livraria.estoque.service.EstoqueService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/estoque")
public class EstoqueController {

    private final EstoqueService estoqueService;

    public EstoqueController(EstoqueService estoqueService) {
        this.estoqueService = estoqueService;
    }

    @GetMapping("/{productId}")
    public EstoqueInfo getEstoque(@PathVariable Long productId) {
        return estoqueService.getEstoqueByProduto(productId);
    }
}
