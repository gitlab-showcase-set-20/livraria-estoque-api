package io.showcase.livraria.estoque.service;

import io.showcase.livraria.estoque.dto.EstoqueInfo;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EstoqueServiceTest {

    private EstoqueService estoqueService = new EstoqueService();

    @Test
    public void deveriaRetornarEstoqueTest() {
        EstoqueInfo estoqueInfo = estoqueService.getEstoqueByProduto(1L);

        assertEquals(1, estoqueInfo.getProductId().longValue());
        assertEquals(0, estoqueInfo.getQuantity().longValue());
    }

    @Test
    public void deveriaRetornarEstoqueTestQuandoId99() {
        EstoqueInfo estoqueInfo = estoqueService.getEstoqueByProduto(99L);

        assertEquals(99, estoqueInfo.getProductId().longValue());
        assertEquals(99, estoqueInfo.getQuantity().longValue());
    }

}
