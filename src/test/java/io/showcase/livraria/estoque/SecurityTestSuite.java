package io.showcase.livraria.estoque;

import io.showcase.livraria.estoque.security.SecurityTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({SecurityTests.class})
public class SecurityTestSuite {
}
